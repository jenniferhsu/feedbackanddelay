/*
  ==============================================================================

  This is an automatically generated GUI class created by the Introjucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Introjucer version: 3.1.0

  ------------------------------------------------------------------------------

  The Introjucer is part of the JUCE library - "Jules' Utility Class Extensions"
  Copyright 2004-13 by Raw Material Software Ltd.

  ==============================================================================
*/

//[Headers] You can add your own extra header files here...
//[/Headers]

#include "PluginEditor.h"


//[MiscUserDefs] You can add your own user definitions and misc code here...
//[/MiscUserDefs]

//==============================================================================
dubshapeAudioProcessorEditor::dubshapeAudioProcessorEditor (DubshapeAudioProcessor* ownerFilter)
    : AudioProcessorEditor(ownerFilter)
{
    addAndMakeVisible (toggleButton = new ToggleButton ("squareOrWTable"));
    toggleButton->setButtonText ("SQUARE");
    toggleButton->addListener (this);
    toggleButton->setColour (ToggleButton::textColourId, Colour (0xffa8a8a8));

    addAndMakeVisible (methodLabel = new Label ("methodLabel",
                                                "METHOD"));
    methodLabel->setFont (Font (15.00f, Font::plain));
    methodLabel->setJustificationType (Justification::centredLeft);
    methodLabel->setEditable (false, false, false);
    methodLabel->setColour (Label::textColourId, Colours::white);
    methodLabel->setColour (TextEditor::textColourId, Colours::black);
    methodLabel->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (wavetableLabel = new Label ("wavetableLabel",
                                                   "WAVETABLE"));
    wavetableLabel->setFont (Font (15.00f, Font::plain));
    wavetableLabel->setJustificationType (Justification::centredLeft);
    wavetableLabel->setEditable (false, false, false);
    wavetableLabel->setColour (Label::textColourId, Colours::white);
    wavetableLabel->setColour (TextEditor::textColourId, Colours::black);
    wavetableLabel->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (readerLabel = new Label ("readerLabel",
                                                "READER"));
    readerLabel->setFont (Font (15.00f, Font::plain));
    readerLabel->setJustificationType (Justification::centredLeft);
    readerLabel->setEditable (false, false, false);
    readerLabel->setColour (Label::textColourId, Colours::white);
    readerLabel->setColour (TextEditor::textColourId, Colours::black);
    readerLabel->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (wavetableBox = new ComboBox ("wavetableBox"));
    wavetableBox->setEditableText (false);
    wavetableBox->setJustificationType (Justification::centredLeft);
    wavetableBox->setTextWhenNothingSelected (String::empty);
    wavetableBox->setTextWhenNoChoicesAvailable ("(no choices)");
    wavetableBox->addItem ("sine,", 1);
    wavetableBox->addItem ("cos,", 2);
    wavetableBox->addItem ("saw,", 3);
    wavetableBox->addItem ("noise", 4);
    wavetableBox->addListener (this);

    addAndMakeVisible (readerBox = new ComboBox ("readerBox"));
    readerBox->setEditableText (false);
    readerBox->setJustificationType (Justification::centredLeft);
    readerBox->setTextWhenNothingSelected (String::empty);
    readerBox->setTextWhenNoChoicesAvailable ("(no choices)");
    readerBox->addItem ("sine,", 1);
    readerBox->addItem ("cos,", 2);
    readerBox->addItem ("saw,", 3);
    readerBox->addItem ("noise", 4);
    readerBox->addListener (this);


    //[UserPreSize]
    //[/UserPreSize]

    setSize (300, 200);


    //[Constructor] You can add your own custom stuff here..
    startTimer(200);    //starts timer with interval of 200mS
    //[/Constructor]
}

dubshapeAudioProcessorEditor::~dubshapeAudioProcessorEditor()
{
    //[Destructor_pre]. You can add your own custom destruction code here..
    //[/Destructor_pre]

    toggleButton = nullptr;
    methodLabel = nullptr;
    wavetableLabel = nullptr;
    readerLabel = nullptr;
    wavetableBox = nullptr;
    readerBox = nullptr;


    //[Destructor]. You can add your own custom destruction code here..
    //[/Destructor]
}

//==============================================================================
void dubshapeAudioProcessorEditor::paint (Graphics& g)
{
    //[UserPrePaint] Add your own custom painting code here..
    //[/UserPrePaint]

    g.fillAll (Colour (0xff070f2d));

    //[UserPaint] Add your own custom painting code here..
    //[/UserPaint]
}

void dubshapeAudioProcessorEditor::resized()
{
    toggleButton->setBounds (96, 16, 150, 24);
    methodLabel->setBounds (24, 16, 64, 24);
    wavetableLabel->setBounds (24, 42, 88, 24);
    readerLabel->setBounds (24, 72, 56, 24);
    wavetableBox->setBounds (120, 48, 72, 16);
    readerBox->setBounds (87, 74, 72, 16);
    //[UserResized] Add your own custom resize handling here..
    //[/UserResized]
}

void dubshapeAudioProcessorEditor::buttonClicked (Button* buttonThatWasClicked)
{
    //[UserbuttonClicked_Pre]
    //[/UserbuttonClicked_Pre]

    if (buttonThatWasClicked == toggleButton)
    {
        //[UserButtonCode_toggleButton] -- add your button handler code here..
        //[/UserButtonCode_toggleButton]
    }

    //[UserbuttonClicked_Post]
    //[/UserbuttonClicked_Post]
}

void dubshapeAudioProcessorEditor::comboBoxChanged (ComboBox* comboBoxThatHasChanged)
{
    //[UsercomboBoxChanged_Pre]
    //[/UsercomboBoxChanged_Pre]

    if (comboBoxThatHasChanged == wavetableBox)
    {
        //[UserComboBoxCode_wavetableBox] -- add your combo box handling code here..
        //[/UserComboBoxCode_wavetableBox]
    }
    else if (comboBoxThatHasChanged == readerBox)
    {
        //[UserComboBoxCode_readerBox] -- add your combo box handling code here..
        //[/UserComboBoxCode_readerBox]
    }

    //[UsercomboBoxChanged_Post]
    //[/UsercomboBoxChanged_Post]
}



//[MiscUserCode] You can add your own definitions of your custom methods or any other code here...
void dubshapeAudioProcessorEditor::timerCallback()
{
     DubshapeAudioProcessor* ourProcessor = getProcessor();
     //exchange any data you want between UI elements and the Plugin "ourProcessor"
}
//[/MiscUserCode]


//==============================================================================
#if 0
/*  -- Introjucer information section --

    This is where the Introjucer stores the metadata that describe this GUI layout, so
    make changes in here at your peril!

BEGIN_JUCER_METADATA

<JUCER_COMPONENT documentType="Component" className="dubshapeAudioProcessorEditor"
                 componentName="" parentClasses="public AudioProcessorEditor, public Timer"
                 constructorParams="dubshapeAudioProcessor* ownerFilter" variableInitialisers="AudioProcessorEditor(ownerFilter)"
                 snapPixels="8" snapActive="1" snapShown="1" overlayOpacity="0.330"
                 fixedSize="0" initialWidth="300" initialHeight="200">
  <BACKGROUND backgroundColour="ff070f2d"/>
  <TOGGLEBUTTON name="squareOrWTable" id="a577f219d11bbafe" memberName="toggleButton"
                virtualName="" explicitFocusOrder="0" pos="96 16 150 24" txtcol="ffa8a8a8"
                buttonText="SQUARE" connectedEdges="0" needsCallback="1" radioGroupId="0"
                state="0"/>
  <LABEL name="methodLabel" id="1e8733b70299cf78" memberName="methodLabel"
         virtualName="" explicitFocusOrder="0" pos="24 16 64 24" textCol="ffffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="METHOD" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <LABEL name="wavetableLabel" id="99908e290ae6c898" memberName="wavetableLabel"
         virtualName="" explicitFocusOrder="0" pos="24 42 88 24" textCol="ffffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="WAVETABLE" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <LABEL name="readerLabel" id="d039f66ea7b9e487" memberName="readerLabel"
         virtualName="" explicitFocusOrder="0" pos="24 72 56 24" textCol="ffffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="READER" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <COMBOBOX name="wavetableBox" id="bd2c6b325a078f98" memberName="wavetableBox"
            virtualName="" explicitFocusOrder="0" pos="120 48 72 16" editable="0"
            layout="33" items="sine,&#10;cos,&#10;saw,&#10;noise" textWhenNonSelected=""
            textWhenNoItems="(no choices)"/>
  <COMBOBOX name="readerBox" id="76d4f915aeba16a1" memberName="readerBox"
            virtualName="" explicitFocusOrder="0" pos="87 74 72 16" editable="0"
            layout="33" items="sine,&#10;cos,&#10;saw,&#10;noise" textWhenNonSelected=""
            textWhenNoItems="(no choices)"/>
</JUCER_COMPONENT>

END_JUCER_METADATA
*/
#endif


//[EndFile] You can add extra defines here...
//[/EndFile]
