/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic startup code for a Juce application.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"


//==============================================================================
DubshapeAudioProcessor::DubshapeAudioProcessor()
{
    // fill in wavetable
    for( int i = 0; i < WTABLELEN; i++ )
    {
        m_wtable[i] = sin(2.0f*M_PI*220.0f*((float)i/WTABLELEN));
    }
}

DubshapeAudioProcessor::~DubshapeAudioProcessor()
{
}

//==============================================================================
const String DubshapeAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

int DubshapeAudioProcessor::getNumParameters()
{
    return 0;
}

float DubshapeAudioProcessor::getParameter (int index)
{
    return 0.0f;
}

void DubshapeAudioProcessor::setParameter (int index, float newValue)
{
}

const String DubshapeAudioProcessor::getParameterName (int index)
{
    return String::empty;
}

const String DubshapeAudioProcessor::getParameterText (int index)
{
    return String::empty;
}

const String DubshapeAudioProcessor::getInputChannelName (int channelIndex) const
{
    return String (channelIndex + 1);
}

const String DubshapeAudioProcessor::getOutputChannelName (int channelIndex) const
{
    return String (channelIndex + 1);
}

bool DubshapeAudioProcessor::isInputChannelStereoPair (int index) const
{
    return true;
}

bool DubshapeAudioProcessor::isOutputChannelStereoPair (int index) const
{
    return true;
}

bool DubshapeAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool DubshapeAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool DubshapeAudioProcessor::silenceInProducesSilenceOut() const
{
    return false;
}

double DubshapeAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int DubshapeAudioProcessor::getNumPrograms()
{
    return 0;
}

int DubshapeAudioProcessor::getCurrentProgram()
{
    return 0;
}

void DubshapeAudioProcessor::setCurrentProgram (int index)
{
}

const String DubshapeAudioProcessor::getProgramName (int index)
{
    return String::empty;
}

void DubshapeAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void DubshapeAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
}

void DubshapeAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

void DubshapeAudioProcessor::processBlock (AudioSampleBuffer& buffer, MidiBuffer& midiMessages)
{
    // This is the place where you'd normally do the guts of your plugin's
    // audio processing...
    for( int channel = 0; channel < getNumInputChannels(); ++channel )
    {
        float* channelData = buffer.getSampleData(channel);
        
        // squaring the input
        /*
        for( int i = 0; i < buffer.getNumSamples(); i++ )
        {
            // amplify the signal
            float ampSamp = channelData[i]*2;
            // square the signal
            channelData[i] = ampSamp*ampSamp;   
        }
         */
        // read from wavetable
        for( int i = 0; i < buffer.getNumSamples(); i++ )
        {
            int  ind = floor(((channelData[i]+1.0f)/2.0f) * WTABLELEN);
            channelData[i] = m_wtable[ind];
        }
    }

    // In case we have more outputs than inputs, we'll clear any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    for (int i = getNumInputChannels(); i < getNumOutputChannels(); ++i)
    {
        buffer.clear (i, 0, buffer.getNumSamples());
    }
}

//==============================================================================
bool DubshapeAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* DubshapeAudioProcessor::createEditor()
{
    return new dubshapeAudioProcessorEditor (this);
}

//==============================================================================
void DubshapeAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void DubshapeAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new DubshapeAudioProcessor();
}
