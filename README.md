feedbackAndDelay

author: Jennifer Hsu (jsh008@ucsd.edu)
date: winter 2015

This is a collection of my experiments on the concepts discussed in Miller's Winter 2015 Music 206 class (feedback and distortion).  This is mostly so that I can make sure I understand the concepts and to refresh myself on writing VSTs and iOS apps.  

Here is a list of the mini-projects:

* week1: waveshaper
